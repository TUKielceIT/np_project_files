# NP_project_files

Files needed for network programming


Case1- create. Utworzyć plik do zbierania danych osobistych. Zadanie polega na napisaniu programu, który wykorzysta podane przez użytkownika dane do wyświetlenia stosownego komunikatu 

Plik: Case1_Script to Collect Personal Information.png


Case2- upgrade. Użytkownik podaje numer dwóch wykorzystywanych VLAN'ów. W zadaniu należy dopisać część kodu, która będzie odpowiadać za sprawdzenie czy wykorzystane numery VLAN'ów różnią się od siebie

Plik: Case2_Compare VLAN.py


Case3- troubleshooting. Celem programu jest sprawdzenie czy podany numer listy kontroli dostępu zalicza sie do standardowych ACL (Access Control List) czy do rozszerzonych. Program należy sprawdzić pod kątem działania.

Plik: Case3_Troubleshooting_ACL.py


Case4- upgrade. Program wyświetla wyniki dla zadanej ilosci rekordów. Działa jednak w nieskończonej pętli. Należy dodać kod który umożliwi przerwanie działania po wpisaniu "quit" lub "q"

Plik: Case4_unix-like quit command.py


Case5- praca z plikami. Otrzymaliście plik z listą urządzeń oraz skrypt do ich wyświetlania. Należy wykonać następujące kroki: 
-uruchomić plik w jupyter notebook
-usunąć puste linie między wierszami
-wykorzystać listę do przeniesienia zawartości pliku. Często przy modyfikacjach plików zewnętrznych zaleca się wprowadzenie modyfikacji na zmiennych lokalnych, które można będzie modyfikować bez ryzyka uszkodzenia pliku.

Plik: Case5_external file.py, devices.txt

Dodatkowo należy napisać skrypt umożliwiający dodawanie nowych urządzeń do listy w pliku .txt. W tym celu należy:
-otworzyć plik w trybie "append"
-wykorzystać funkcję input() wewnątrz pętli while
-wykorzystać instrukcję warunkową do przerwania działania programu w chwili kiedy użytkownik wpisze "exit".
-wyświetlić stosowny komunikat przerwania
-użyć komendy "file.write(new_item + “\n”)" w celu dodania nowego urządzenia

Plik: Case5_external file.png, devices.txt